const axios = require("axios");

export function getTopRated() {
  // Make a request for a user with a given ID
  return axios
    .get(
      "https://api.themoviedb.org/3/movie/top_rated?api_key=688d50dc15170b98c9ce83093569b0c2"
    )
    .then(response => {
      // console.log("response data: ", JSON.stringify(response.data.results));
      return response.data.results;
    })
    .catch(error => {
      console.log("error: ", error);
      console.log("response header: ", error.headers);
    })
    .finally(() => {
      // always executedv
    });
}

export function getPopular() {
  // Make a request for a user with a given ID
  return axios
    .get(
      "https://api.themoviedb.org/3/movie/popular?api_key=688d50dc15170b98c9ce83093569b0c2"
    )
    .then(response => {
      // console.log("response data: ", JSON.stringify(response.data.results));
      return response.data.results;
    })
    .catch(error => {
      console.log("error: ", error);
      console.log("response header: ", error.headers);
    })
    .finally(() => {
      // always executedv
    });
}

export function getNowPlaying() {
  // Make a request for a user with a given ID
  return axios
    .get(
      "https://api.themoviedb.org/3/movie/now_playing?api_key=688d50dc15170b98c9ce83093569b0c2"
    )
    .then(response => {
      // console.log("response data: ", JSON.stringify(response.data.results));
      return response.data.results;
    })
    .catch(error => {
      console.log("error: ", error);
      console.log("response header: ", error.headers);
    })
    .finally(() => {
      // always executedv
    });
}

export function getSearchedMovie(queryText, page) {
  // Make a request for a user with a given ID

  const url =
    "https://api.themoviedb.org/3/search/movie?api_key=688d50dc15170b98c9ce83093569b0c2&query=" +
    queryText +
    "&page=" +
    page;

  return axios
    .get(url)
    .then(response => {
      // console.log("response data: ", JSON.stringify(response.data.results));
      return response.data.results;
      // response.total_pages
    })
    .catch(error => {
      console.log("error: ", error);
      console.log("response header: ", error.headers);
    })
    .finally(() => {
      // always executedv
    });
}

// eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2ODhkNTBkYzE1MTcwYjk4YzljZTgzMDkzNTY5YjBjMiIsInN1YiI6IjVlMzFjZTlkMzI2YzE5MDAxMjFlOWYxZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ._8heXFnLzzhYD1su62nhJZ1PzrktUrfheoCi_S3JjFo
