import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// —------------------------------------------------------------------------—

class ContactMe extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—

  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  // —---------------------------------—
  // Rendering  submethods
  // —---------------------------------—

  // —---------------------------------—

  render() {
    return <div className={this.props.className}>Contact me</div>;
  }
}

// —------------------------------------------------------------------------—

const StyledContactMe = styled(ContactMe)`
  & {
  }
`;
export default StyledContactMe;
