import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./Home/index.js";
import ContactMe from "./ContactMe/index.js";

// —------------------------------------------------------------------------—

class IndexPage extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—

  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  // —---------------------------------—
  // Rendering  submethods
  // —---------------------------------—

  // —---------------------------------—

  render() {
    return (
      <Router>
        <div className={this.props.className}>
          {/* <Home /> */}
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/contactMe" exact component={ContactMe} />
            <Route path="/contactMe" exact component={ContactMe} />
            <Route path="/contactMe" exact component={ContactMe} />
          </Switch>
        </div>
      </Router>
    );
  }
}

// —------------------------------------------------------------------------—

const StyledIndexPage = styled(IndexPage)`
  & {
  }
`;
export default StyledIndexPage;
