import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button } from "antd";

import { FaHeart } from "react-icons/fa";
// —------------------------------------------------------------------------—

class SearchItem extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    width: PropTypes.string.isRequired,
    height: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    posterPath: PropTypes.string.isRequired,
    voteAverage: PropTypes.string.isRequired,
    releaseDate: PropTypes.string.isRequired,
    parentCallback: PropTypes
  };

  state = {
    prevPosition: 0,
    translation: 0,
    styleProps: {},
    isVisible: "hidden"
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—

  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  // —---------------------------------—
  // Rendering  submethods
  // —---------------------------------—

  // —---------------------------------—

  render() {
    const {
      data,
      key,
      prevPosition,
      title,
      posterPath,
      voteAverage,
      releaseDate
    } = this.props;
    const { translation, styleProps, isVisible } = this.state;

    const imagePath = "http://image.tmdb.org/t/p/w342/" + posterPath;

    const slideProps = {
      style: {
        transitionDuration: "2s",
        backgroundImage: `url(${imagePath})`
      }
    };

    return (
      <div className={this.props.className}>
        <div
          className="container-item"
          // ref={this.state && this.state.containerRef}
          onMouseEnter={() => {
            this.setState({ isVisible: "visible" });
          }}
          onMouseLeave={() => {
            this.setState({ isVisible: "hidden" });
          }}
          {...slideProps}
        >
          <div
            className="containerHover-item"
            style={{
              visibility: isVisible,
              transition: `visibility 0s linear 200ms`
            }}
          >
            <div className="container-title">{title}</div>
            <div>
              <div className="container-icon">
                <FaHeart className="icon" />
                <p className="vote"> {voteAverage}</p>
              </div>
              <div className="container-releaseDate">
                <p className="releaseDate"> {releaseDate}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// —------------------------------------------------------------------------—

const StyledSearchItem = styled(SearchItem)`
  & {
    .container-item {
      height: 400px;
      width: ${props => props.width}px;
      font-size: 25px;
      display: inline-block;
      ${"" /* line-height: 100px; */}
      z-index: 0;
      background-size: cover;
      opacity: 1;
      display: flex;
      position: relative;
      margin-top: 8%;
    }

    .containerHover-item {
      border: 1px solid black;
      width: 100%;
      background-color: black;
      opacity: 0.9;
      white-space: normal;
      :hover {
        transition-duration: 2s;
        animation-fill-mode: forwards;
      }
    }

    .container-title {
      display: flex;
      justify-content: center;
      height: 100%;
      align-items: center;
      font-size: large;
      color: white;
      font-weight: bold;
      font-family: monospace;
      font-variant: all-petite-caps;
      font-weight: 900;
      font-size: 23px;
      word-break: break-word;
      line-height: normal;
      text-align: center;
    }

    .container-icon {
      position: absolute;
      bottom: 5px;
      left: 5px;
      color: white;
      display: flex;
      flex-direction: row;

      ${"" /* position: relative;
      bottom: 21px;
      left: 5px;
      color: white;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: row;
      -ms-flex-direction: row;
      flex-direction: row; */}
    }

    .icon {
      font-size: 18px;
    }

    .vote {
      margin: 0px;
      margin-left: 7px;
      font-size: 16px;
      font-family: monospace;
    }

    .container-releaseDate {
      position: absolute;
      bottom: 5px;
      right: 7px;
      color: white;

      ${"" /* position: relative;
      bottom: 45px;
      right: -177px;
      color: white; */}
    }

    .releaseDate {
      color: white;
      font-size: 16px;
      margin: 0;
      font-family: monospace;
    }
  }
`;
export default StyledSearchItem;
