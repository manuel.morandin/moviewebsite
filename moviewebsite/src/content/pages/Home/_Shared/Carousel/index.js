import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import CarouselItem from "./CarouselItem/index.js";

// —------------------------------------------------------------------------—

class Carousel extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
  };
  state = {
    items: [],
    currentTranslation: 0,
    newTranslation: 0,
    direction: 0,
    hasPrev: false,
    hasNext: true,
    width: 0
  };

  componentWillMount() {
    const { data } = this.state;

    console.log(
      "asfmnsadfkhbadsiukfgbwadzhlfgvcwaskldhmgfdakhxmgfblkhsdgb: ",
      this.props
    );
    this.setState({ width: window.screen.width });

    this.setState({ items: data, itemsNumber: data && data.length });

    // console.log(
    //   "asfmnsadfkhbadsiukfgbwadzhlfgvcwaskldhmgfdakhxmgfblkhsdgb: ",
    //   this.props
    // );
    this.setState({ width: window.screen.width });
    if (this.props && this.props.data) {
      this.setState({
        items: this.props && this.props.data,
        itemsNumber: data && data.length
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { data } = nextProps;

    console.log("data: ", data);
    console.log("nextProps: ", this.nextProps);

    this.setState({ items: data, itemsNumber: data && data.length });
  }

  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  scroll = direction => {
    const { currentTranslation, itemsNumber } = this.state;

    var w = this.getWidth();

    if (direction == 1) {
      this.setState({
        currentTranslation: currentTranslation + w[0],
        newTranslation: -w[0],
        direction: 1,
        hasPrev: currentTranslation + w[0] == 0 ? false : true,
        hasNext:
          currentTranslation + w[0] >= (w[0] / w[1]) * itemsNumber - w[0]
            ? false
            : true //controllo se la lunghezza dello schermo è maggiore
      });
    } else {
      this.setState({
        currentTranslation: currentTranslation - w[0],
        newTranslation: w[0],
        direction: -1,
        hasPrev: currentTranslation - w[0] == 0 ? false : true,
        hasNext:
          currentTranslation - w[0] >= (w[0] / w[1]) * itemsNumber - w[0]
            ? false
            : true //controllo se la lunghezza dello schermo è maggiore
      });
    }
  };

  callbackFunction = childData => {
    this.setState({ message: childData });
  };

  getWidth = () => {
    let results;
    if (window.screen.width <= 425) {
      results = [window.screen.width, 1];
    }

    if (window.screen.width > 425 && window.screen.width <= 768) {
      results = [window.screen.width / 4, 4];
    }

    if (window.screen.width > 769) {
      results = [window.screen.width / 6, 6];
    }

    return results;
  };

  // —---------------------------------—

  render() {
    const { newTranslation, direction, items } = this.state;

    let w = this.getWidth();
    console.log("IN CAROUSEL DIO NATO: ");
    console.log("this.state: ", this.state);
    console.log("this.state.items: ", this.state.items);

    const { title } = this.props;

    return (
      <div className={this.props.className}>
        <div className="borderTop" />
        <p className="title">{title}</p>

        <div className="container">
          {this.state.hasPrev && (
            <a
              className="leftArrow"
              onClick={() => {
                this.scroll(-1);
              }}
            >
              <div className="arrowContainer">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="chevron-left"
                  class="svg-inline--fa fa-chevron-left fa-w-10"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 320 512"
                >
                  <path
                    fill="white"
                    d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"
                  ></path>
                </svg>
              </div>
            </a>
          )}

          {this.state &&
            this.state.items &&
            this.state.items.map((item, index) => (
              <CarouselItem
                key={index}
                width={w[0]}
                height="200px"
                parentCallback={this.callbackFunction}
                translation={newTranslation * w[1]}
                direction={direction}
                id={item.id}
                title={item.title}
                posterPath={item.poster_path}
                voteAverage={item.vote_average}
                releaseDate={item.release_date}
              />
            ))}

          {this.state.hasNext && (
            <a
              className="rightArrow"
              onClick={() => {
                this.scroll(1);
              }}
            >
              <div className="arrowContainer">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="chevron-right"
                  class="svg-inline--fa fa-chevron-right fa-w-10"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 320 512"
                >
                  <path
                    fill="white"
                    d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
                  ></path>
                </svg>
              </div>
            </a>
          )}
        </div>
      </div>
    );
  }
}

// —------------------------------------------------------------------------—

const StyledCarousel = styled(Carousel)`
  & {
    .container {
      vertical-align: middle;
      display: flex;
      height: 400px;
      width: 100%;
      white-space: nowrap;
      margin-top: 20px;
      margin-bottom: 20px;

      overflow-x: hidden;
      overflow-y: hidden;
      transform: translateX(${state => state.translation}%);
    }

    .borderTop {
      height: 5px;
      width: 16%;
      background-color: white;
      margin-left: 20px;
    }

    .title {
      margin-left: 20px;
      font-size: x-large;
      font-weight: 900;
      color: white;
      font-family: sans-serif;
      margin-top: 11px;
    }

    .leftArrow {
      position: absolute;
      width: 50px;
      height: 400px;
      display: flex;
      z-index: 1;
    }

    .leftArrow:hover {
      background: linear-gradient(to right bottom, #2b2b2b, #000000);
      opacity: 0.6;
      z-index: 1;
    }

    .rightArrow {
      position: absolute;
      width: 50px;
      right: 0px;
      height: 400px;
      display: flex;
    }

    .rightArrow:hover {
      background: linear-gradient(to right bottom, #2b2b2b, #000000);
      opacity: 0.6;
      z-index: 1;
    }

    .arrowContainer {
      flex: 1;
      align-self: center;
      z-index: 2;
    }
  }
`;
export default StyledCarousel;
