import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import CarouselContext from "../_Shared/CarouselContext.js";
import setSizeElement from "../_Shared/setSizeElement.js";
import useSliding from "../_Shared/useSliding.js";

import * as api from "../../../../../../utils/api.js";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fo } from "@fortawesome/free-solid-svg-icons";
import { FaHeart } from "react-icons/fa";

// —------------------------------------------------------------------------—

class CarouselItem extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    width: PropTypes.string.isRequired,
    height: PropTypes.string.isRequired,
    translation: PropTypes.string.isRequired,
    direction: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    posterPath: PropTypes.string.isRequired,
    voteAverage: PropTypes.string.isRequired,
    releaseDate: PropTypes.string.isRequired,
    parentCallback: PropTypes
  };

  state = {
    prevPosition: 0,
    translation: 0,
    styleProps: {},
    isVisible: "hidden"
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—

  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log("this.state.translation: ", this.state.translation);
    console.log("nextProps.translation: ", nextProps.translation);

    this.setState({
      prevPosition: this.state.translation,
      translation: this.state.translation + nextProps.translation,
      direction: nextProps.direction
    });
  }

  // —---------------------------------—
  // Rendering  submethods
  // -------------------------------------

  // —---------------------------------—

  render() {
    const {
      data,
      key,
      prevPosition,
      title,
      posterPath,
      voteAverage,
      releaseDate
    } = this.props;
    const { translation, direction, styleProps, isVisible } = this.state;
    console.log("translation: ", translation);
    const imagePath = "http://image.tmdb.org/t/p/w342/" + posterPath;

    const slideProps = {
      style: {
        transform: `translate3d(${translation}px, 0, 0)`,
        transitionDuration: "2s",
        backgroundImage: `url(${imagePath})`
      }
    };

    return (
      <CarouselContext.Provider value={this.state && this.state.elementRef}>
        <div className={this.props.className}>
          <div
            className="container-item"
            ref={this.state && this.state.containerRef}
            onMouseEnter={() => {
              this.setState({ isVisible: "visible" });
            }}
            onMouseLeave={() => {
              this.setState({ isVisible: "hidden" });
            }}
            {...slideProps}
          >
            <div
              className="containerHover-item"
              style={{
                visibility: isVisible,
                transition: `visibility 0s linear 200ms`
              }}
            >
              <div className="container-title">{title}</div>
              <div className="container-icon">
                <FaHeart className="icon" />
                <p className="vote"> {voteAverage}</p>
              </div>
              <div className="container-releaseDate">
                <p className="releaseDate"> {releaseDate}</p>
              </div>
            </div>
          </div>
        </div>
      </CarouselContext.Provider>
    );
  }
}

// ----------------------------------------------------------------------------

const StyledCarouselItem = styled(CarouselItem)`
  & {
    .container-item {
      height: 400px;
      width: ${props => props.width}px;
      font-size: 25px;
      display: inline-block;
      z-index: 0;
      background-size: cover;
      opacity: 1;
      display: flex;

      ${
        "" /* @media (max-width: 425px) {
          width: (${props => props.width}*6)px;
      }
      @media (max-width: 768px) {
          width: ${props => props.width}px;
      } */
      }

      ${
        "" /* @media (max-width: 768px) {
        flex-direction: column;
        opacity: 1;
      } */
      }
    }

    .containerHover-item {
      ${"" /* visibility: hidden; */}
      border: 1px solid black;
      width: 100%;
      background-color: black;
      opacity: 0.9;
      white-space: normal;
      :hover {
        transition-duration: 2s;
        animation-fill-mode: forwards;
      }
    }



      .container-title {
        display: flex;
        justify-content: center;
        height: 100%;
        align-items: center;
        font-size: large;
        color: white;
        font-weight: bold;
        font-family: monospace;
        font-variant: all-petite-caps;
        font-weight: 900;
        font-size: 23px;
        word-break: break-word;
        line-height: normal;
        text-align: center;
      }

      .container-icon {
        position: absolute;
        bottom: 5px;
        left: 5px;
        color: white;
        display: flex;
        flex-direction: row;
      }

      .icon {
        font-size: 18px;
      }

      .vote {
        margin: 0px;
        margin-left: 7px;
        font-size: 16px;
        font-family: monospace;
      }

      .container-releaseDate{
        position: absolute;
        bottom: 5px;
        right: 7px;
        color: white;
      }

      .releaseDate{
        color: white;
        font-size: 16px;
        margin:0;
        font-family: monospace;
      }
    }
  }
`;
export default StyledCarouselItem;
