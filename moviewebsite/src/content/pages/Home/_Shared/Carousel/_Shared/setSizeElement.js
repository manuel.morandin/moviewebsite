import { useState, useRef, useEffect } from "react";

const useSizeElement = () => {
  const elementRef = useRef(null);
  const [position, setPosition] = useState(0);

  useEffect(() => {
    setPosition(elementRef.current.clientPosition);
  }, [elementRef.current]);

  return { position, elementRef };
};

export default useSizeElement;
