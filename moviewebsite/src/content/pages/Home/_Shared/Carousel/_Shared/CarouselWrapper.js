import React from "react";
import styled from "styled-components";

// —------------------------------------------------------------------------—

const CarouselWrapper = ({ children }) => (
  <div className="slider-wrapper">{children}</div>
);

// —------------------------------------------------------------------------—

const StyledCarouselWrapper = styled(CarouselWrapper)`
  & {
    .slider-wrapper {
      padding: 40px 0;
      overflow: hidden;
      position: relative;
    }
  }
`;
export default StyledCarouselWrapper;
