import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import "antd/dist/antd.css";
import { Pagination } from "antd";

import * as api from "../../../utils/api.js";

import Header from "../../components/Header/index.js";
import Carousel from "./_Shared/Carousel/index.js";

import SearchItem from "./_Shared/SearchItem/index.js";
// —------------------------------------------------------------------------—

class Home extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired
  };

  state = {
    searchString: "",
    searchData: {},
    data: {},
    dataPopular: {},
    dataNowPlaying: {},
    isLoadingData: false
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—
  componentDidMount() {
    Promise.all([
      api.getTopRated(),
      api.getPopular(),
      api.getNowPlaying()
    ]).then(([res1, res2, res3]) => {
      this.setState({
        data: res1,
        dataPopular: res2,
        dataNowPlaying: res3
      });
    });
  }
  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  getReturnedData = (data, searchString) => {
    console.log("DATI ARRIVATI: ", data);
    console.log("searchString: ", searchString);

    if (data != false) {
      this.setState({
        isLoadingData: true,
        searchData: data,
        searchString: searchString
      });
    } else {
      this.setState({
        isLoadingData: false
      });
    }
    // else {
    //   console.log("XXXX");
    //   Promise.all([
    //     api.getTopRated(),
    //     api.getPopular(),
    //     api.getNowPlaying()
    //   ]).then(([res1, res2, res3]) => {
    //     console.log("res1: ", res1);
    //     console.log("res2: ", res2);
    //     console.log("res3: ", res3);
    //
    //     this.setState({
    //       data: res1,
    //       dataPopular: res2,
    //       dataNowPlaying: res3,
    //       isLoadingData: false,
    //       searchString: searchString
    //     });
    //   });
    // }
  };

  isPageChanged = pageNumber => {
    api.getSearchedMovie(pageNumber).then(data => {
      this.setState({
        isLoadingData: true,
        searchData: data
      });
    });
  };

  // —---------------------------------—
  // Rendering  submethods
  // —---------------------------------—

  isActiveSearch = () => {
    return (
      <div>
        <div className="containerSearched">
          {this.state &&
            this.state.searchData &&
            this.state.searchData.map((item, index) => (
              <SearchItem
                key={index}
                width={"280"}
                height="200px"
                parentCallback={this.callbackFunction}
                id={item.id}
                title={item.title}
                posterPath={item.poster_path}
                voteAverage={item.vote_average}
                releaseDate={item.release_date}
              />
            ))}
        </div>
        <div className="paginator-container">
          <Pagination
            defaultCurrent={1}
            total={50}
            onChange={res => this.isPageChanged(res)}
          />
        </div>
      </div>
    );
  };

  isNotActiveSearch = () => {
    const { data, dataPopular, dataNowPlaying } = this.state;

    console.log("IN NOT ACTIVE|!!!!!");

    console.log("data: ", data);
    console.log("dataPopular: ", dataPopular);
    console.log("dataNowPlaying: ", dataNowPlaying);

    console.log("1: ", this.state.data);
    console.log("2: ", this.state.dataPopular);
    console.log("3: ", this.state.dataNowPlaying);
    return (
      <div className="containerAll">
        <Carousel data={data} title={"Top viewed"} />
        <Carousel data={dataPopular} title={"Popular"} />
        <Carousel data={dataNowPlaying} title={"Now playing"} />
      </div>
    );
  };

  // —---------------------------------—

  render() {
    const { isLoadingData } = this.state;

    let content;
    if (isLoadingData) {
      content = this.isActiveSearch();
    } else {
      content = this.isNotActiveSearch();
    }
    return (
      <div className={this.props.className}>
        <Header getReturnedData={this.getReturnedData} />
        {content}
      </div>
    );
  }
}

// —------------------------------------------------------------------------—

const StyledHome = styled(Home)`
  & {
    .containerAll {
      background-color: rgb(45, 45, 45);
      border: 2px solid blue;
    }

    .containerSearched {
      background-color: rgb(45, 45, 45);
      display: flex;
      flex-wrap: wrap;
      justify-content: space-around;
    }

    .paginator-container {
      background-color: rgb(45, 45, 45);
      display: flex;
      flex: 1;
      padding-top: 3%;
      padding-bottom: 3%;
      justify-content: center;
    }
  }
`;
export default StyledHome;
