import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { Link } from "react-router-dom";

import * as api from "../../../utils/api.js";
// —------------------------------------------------------------------------—

class Header extends React.Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    getReturnedData: PropTypes.func.isRequired,
    getReturnedSearchString: PropTypes.func.isRequired
  };

  state = {
    data: {},
    searchValue: ""
  };

  // —---------------------------------—
  // Lifecycle methods
  // —---------------------------------—

  // —---------------------------------—
  // Component logic methods
  // —---------------------------------—

  // —---------------------------------—
  // Rendering  submethods
  // —---------------------------------—

  handleInputChange = e => {
    this.setState({ searchValue: e.target.value });
    if (e.target.value != "") {
      api.getSearchedMovie(e.target.value, 1).then(response => {
        this.setState({ data: response });
        this.props.getReturnedData(response, e.target && e.target.value);
      });
    } else {
      this.props.getReturnedData(false);
    }
  };
  // —---------------------------------—

  render() {
    const { searchValue } = this.state;
    return (
      <div className={this.props.className}>
        <div className="navbar">
          <input
            type="text"
            name="name"
            placeholder="Search"
            className="searchInput"
            onChange={this.handleInputChange}
            value={searchValue}
          />
        </div>
      </div>
    );
  }
}

// —------------------------------------------------------------------------—

const StyledHeader = styled(Header)`
  & {
    .navbar {
      display: flex;
      flex-direction: row;
      justify-content: space-around;
      align-items: center;
      min-height: 10vh;
      background-color: rgb(62, 61, 61);
      border: 2px solid red;
    }

    .searchInput {
      width: 90%;
      height: 40px;
      border-radius: 8px;
      background-color: rgb(80, 80, 80);
      border: 0;
      font-size: 20px;
      color: white;
      padding: 6px;
    }
  }
`;
export default StyledHeader;
